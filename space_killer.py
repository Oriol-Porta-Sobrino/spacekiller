import pygame
import random

FPS=60
GAME_TITLE='SpaceKiller'
WINDOW_SIZE_X=800
WINDOW_SIZE_Y=600
WINDOW_CENTER_X=WINDOW_SIZE_X/2
WINDOW_CENTER_Y=WINDOW_SIZE_Y/2
PLAYER_SIZE_X=50
PLAYER_SIZE_Y=56
WINDOW_MARGIN_LIMIT=50
ANIMATION_VELOCITY=200
SPRITES_PER_ANIMATION=4
POSITION_TO_DELETE_PLAYER_SHOT_Y_TOP = -100
POSITION_TO_DELETE_PLAYER_SHOT_Y_BOTTOM = 600
WHITE = (255,255,255)
GAME_STATE_MENU=1
GAME_STATE_PLAYING=2
GAME_STATE_DEATH=3
GAME_STATE_EXIT=4
GAME_STATE_VICTORY=5
POWER_UP_SPEED=-2
GAME_OVER_TEXT='GAME OVER'
ENEMIES=['enemy1', 'enemy2']


def draw_player(screen, player):
    if player['moved']==True:
        sprite=player['sprites'][player['sprite_index']]
    else:
        sprite=player['idle']
    square=sprite.get_rect().move(player['x'], player['y'])
    screen.blit(sprite, square)

def move_player(player, delta):
    moved=False
    vel=int(player['velocity']*delta)
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT] and player['x']>0:
        player['x']=max(player['x']-vel, 0)
        player['sprites']=player['left']
        moved=True
        player['animation_time']
    if keys[pygame.K_RIGHT] and player['x']<WINDOW_SIZE_X-WINDOW_MARGIN_LIMIT:
        player['x']=min(player['x']+vel, WINDOW_SIZE_X-WINDOW_MARGIN_LIMIT)
        player['sprites']=player['right']
        moved=True
    if keys[pygame.K_UP] and player['y']>200:
        player['y']=max(player['y']-vel, 0)
    if keys[pygame.K_DOWN] and player['y']<WINDOW_SIZE_Y-WINDOW_MARGIN_LIMIT:
        player['y']=min(player['y']+vel, WINDOW_SIZE_Y-WINDOW_MARGIN_LIMIT)
    if moved:
        player['moved']=True
        player['animation_time']+=delta
        if player['animation_time']>ANIMATION_VELOCITY:
            if player['sprite_index']<SPRITES_PER_ANIMATION-1:
                player['animation_time']-=ANIMATION_VELOCITY
                player['sprite_index']=(player['sprite_index']+1)%SPRITES_PER_ANIMATION
    else:
        player['moved']=False
        player['sprite_index']=0
        player['animation_time']=0
        

def player_shot(screen, player, player_shots):
    keys = pygame.key.get_pressed()
    if keys[pygame.K_SPACE]:
        if player['time_since_last_shot']>player['shot_colddown']:
            if (player['power_shot_bullets'] > 0):
                player['power_shot_bullets']=player['power_shot_bullets']-1
                shot=player['shots']['power_shot']
            else:
                shot=player['shots']['light_shot']
            player_shots.append(spawn_shot(player, shot))
            player['time_since_last_shot'] = 0
    draw_shot(screen, player_shots)

def draw_shot(screen, player_shots):
    for shot in player_shots:
        sprite=shot['sprite']
        square=sprite.get_rect().move(shot['x'], shot['y'])
        screen.blit(sprite, square)
        

def shot_move(shots):
    for n in range(len(shots)-1, -1, -1):
        shot=shots[n]
        shot['y']=shot['y']-shot['shot_velocity']
        

def spawn_shot(ship, shot):
    x = ship['x'] - shot['x_shot_position']
    y = ship['y'] - shot['y_shot_position']
    sound = shot['sound']
    shot_spawned={
        'sprite': shot['sprite'],
        'collision': shot['shot_collision'],
        'x': x,
        'y': y,
        'damage': shot['damage'],
        'shot_velocity': shot['shot_velocity']
    }
    pygame.mixer.Sound.play(sound)
    return shot_spawned

def create_player():
    player={
        'enemy': False,
        'moved': False,
        'velocity': 0.2,
        'life': 3,
        'right': [
            pygame.image.load('images/ship/right1.png'),
            pygame.image.load('images/ship/right2.png'),
            pygame.image.load('images/ship/right3.png'),
            pygame.image.load('images/ship/right4.png')
        ],
        'left': [
            pygame.image.load('images/ship/left1.png'),
            pygame.image.load('images/ship/left2.png'),
            pygame.image.load('images/ship/left3.png'),
            pygame.image.load('images/ship/left4.png')
        ],
        'idle': pygame.image.load('images/ship/idle.png'),
        'sprite_index': 0,
        'animation_time': 0,
        'invencible_time': 3000,
        'time_since_last_hit': 0,
        'x': WINDOW_CENTER_X-PLAYER_SIZE_X,
        'y': WINDOW_CENTER_Y-PLAYER_SIZE_Y,
        'time_since_last_shot': 0,
        'shot_colddown': 500,
        'shots': {
            'light_shot': {
                'sprite': pygame.image.load('images/laser_sprites/02.png'),
                'shot_velocity': 4,
                'damage': 1,
                'shot_collision': pygame.Rect(52, 52, 19, 27),
                'x_shot_position': 37,
                'y_shot_position':PLAYER_SIZE_Y - 25,
                'sound': pygame.mixer.Sound("sounds/player/player_sound_shot.mp3")
            },
            'power_shot': {
                'sprite': pygame.image.load('images/laser_sprites/01.png'),
                'shot_velocity': 6,
                'damage': 2,
                'shot_collision': pygame.Rect(52, 52, 19, 27),
                'x_shot_position': 37,
                'y_shot_position':PLAYER_SIZE_Y - 25,
                'sound': pygame.mixer.Sound("sounds/player/player_sound_shot.mp3")
            }
        },
        'explosion_sound': pygame.mixer.Sound("sounds/player/explosion_sound.mp3"),
        'hit_sound': pygame.mixer.Sound("sounds/enemies/hit.mp3"),
        'power_shot_bullets': 0
    }
    player['sprites']=player['idle']
    return player

def create_enemies():
    enemies={
        'enemy1': {
            'name': 'enemy1',
            'aproached': False,
            'enemy': True,
            'moved': False,
            'velocity': 4,
            'sprite': pygame.image.load('images/enemies/enemy1/enemy1.png'),
            'blink_sprite': pygame.image.load('images/enemies/enemy1/enemy1blink.png'),
            'life': 2,
            'x': WINDOW_CENTER_X-PLAYER_SIZE_X,
            'y': WINDOW_CENTER_Y-PLAYER_SIZE_Y,
            'x_power_up_position': 21,
            'y_power_up_position': 16,
            'invencible_time': 1000,
            'time_since_last_hit': 0,
            'time_since_last_shot': 0,
            'time_blinking': 1000,
            'blinking_time': 1000,
            'shot_colddown': random.randint(2000, 6000),
            'explosion_sound': pygame.mixer.Sound("sounds/enemies/explosion_sound.mp3"),
            'hit_sound': pygame.mixer.Sound("sounds/enemies/hit.mp3"),
            'shots': {
                'light_shot': {
                    'sprite': pygame.image.load('images/laser_sprites/09.png'),
                    'shot_velocity': -4,
                    'damage': 1,
                    'shot_collision': pygame.Rect(53, 44, 13, 29),
                    'x_shot_position': 34,
                    'y_shot_position':PLAYER_SIZE_Y - 70,
                    'sound': pygame.mixer.Sound("sounds/player/player_sound_shot.mp3")
                }
            }
        },
        'enemy2': {
            'name': 'enemy2',
            'aproached': False,
            'enemy': True,
            'moved': False,
            'velocity': 2,
            'sprite': pygame.image.load('images/enemies/enemy2/enemy2.png'),
            'blink_sprite': pygame.image.load('images/enemies/enemy2/enemy2blink.png'),
            'life': 4,
            'x': WINDOW_CENTER_X-PLAYER_SIZE_X,
            'y': WINDOW_CENTER_Y-PLAYER_SIZE_Y,
            'x_power_up_position': 21,
            'y_power_up_position': 16,
            'invencible_time': 1000,
            'time_since_last_hit': 0,
            'time_since_last_shot': 0,
            'time_blinking': 1000,
            'blinking_time': 1000,
            'shot_colddown': random.randint(2000, 6000),
            'explosion_sound': pygame.mixer.Sound("sounds/enemies/explosion_sound.mp3"),
            'hit_sound': pygame.mixer.Sound("sounds/enemies/hit.mp3"),
            'shots': {
                'light_shot': {
                    'sprite': pygame.image.load('images/laser_sprites/13.png'),
                    'shot_velocity': -3,
                    'damage': 1,
                    'shot_collision': pygame.Rect(32, 18, 9, 76),
                    'x_shot_position': 10,
                    'y_shot_position':PLAYER_SIZE_Y - 70,
                    'sound': pygame.mixer.Sound("sounds/player/player_sound_shot.mp3")
                }
            }
        }    
    }
    return enemies

def create_waves():
    waves=[
        {
            'enemy1': 1,
        },
        {
            'enemy1': 3
        },
        {
            'enemy2': 1
        },
        {
            'enemy2': 3
        },
        {
            'enemy1': 1,
            'enemy2': 1
        },
        {
            'enemy1': 3,
            'enemy2': 3
        }
    ]
    return waves

def check_waves(waves, enemies, enemies_spawned):
    if (len(enemies_spawned)==0 and len(waves)>0):
        pygame.mixer.Sound.play(pygame.mixer.Sound("sounds/effects/new_wave.mp3"))
        wave=waves[0]
        enemies_spawned.extend(spawn_enemies(wave, enemies))
        del waves[0]

def spawn_enemies(wave, enemies): 
    enemies_spawned=[]
    for enemy in ENEMIES:
        if enemy in wave:
            count = wave[enemy]
            for x in range(count):
                if (random.randint(0, 1) == 0):
                    enemy_direction='left'
                else:
                    enemy_direction='right'
                spawned_enemy=enemies[enemy].copy()
                spawned_enemy['x']=random.randint(0, WINDOW_SIZE_X-spawned_enemy['sprite'].get_width())
                spawned_enemy['y']=random.randint(-600, -200)
                spawned_enemy['y_position']=random.randint(20, 120)
                spawned_enemy['direction']=enemy_direction
                enemies_spawned.append(spawned_enemy)
    return enemies_spawned
   
def draw_enemies(screen, enemies_spawned):
    for enemy_spawned in enemies_spawned:
        if (enemy_spawned['blinking_time']<enemy_spawned['time_blinking']):
            sprite=enemy_spawned['blink_sprite']
        else:
            sprite=enemy_spawned['sprite']
        square=sprite.get_rect().move(enemy_spawned['x'], enemy_spawned['y'])
        screen.blit(sprite, square)

def aproach_enemies(enemies_spawned):
    for n in range(len(enemies_spawned)-1, -1, -1):
        enemy_spawned=enemies_spawned[n]
        if (enemy_spawned['y']<enemy_spawned['y_position']):
            enemy_spawned['y']=enemy_spawned['y']+enemy_spawned['velocity']
        else:
            enemy_spawned['aproached'] = True

def move_enemies(enemies_spawned):
    for n in range(len(enemies_spawned)-1, -1, -1):
        enemy_spawned=enemies_spawned[n]
        if enemy_spawned['aproached'] == True:
            if enemy_spawned['direction'] == 'right':
                if enemy_spawned['x']<WINDOW_SIZE_X-enemy_spawned['sprite'].get_width():
                    enemy_spawned['x']=enemy_spawned['x']+enemy_spawned['velocity']
                else:
                    enemy_spawned['direction']='left'
            else:
                if enemy_spawned['x']>0:
                    enemy_spawned['x']=enemy_spawned['x']-enemy_spawned['velocity']
                else:
                    enemy_spawned['direction']='right'

def enemies_shots_function(screen, enemies_spawned, enemies_shots):
    for enemy_spawn in enemies_spawned:
        if enemy_spawn['time_since_last_shot']>enemy_spawn['shot_colddown'] and enemy_spawn['aproached']:
                enemies_shots.append(spawn_shot(enemy_spawn, enemy_spawn['shots']['light_shot']))
                enemy_spawn['time_since_last_shot'] = 0
    draw_shot(screen, enemies_shots)

def hit_ship(ship, shots):
    if (ship['enemy']):
        sprite=ship['sprite']
    else:
        if (ship['moved'] == False):
            sprite=ship['idle']
        else:
            sprite=ship['sprites'][ship['sprite_index']]
    ship_square=sprite.get_rect().move(ship['x'], ship['y'])
    for n in range(len(shots)-1, -1, -1):
        shot=shots[n]
        shot_square=shot['collision'].move(shot['x'], shot['y'])
        if ship_square.colliderect(shot_square) and ship['time_since_last_hit']>ship['invencible_time']:
            pygame.mixer.Sound.play(ship['hit_sound'])
            ship['time_since_last_hit']=0
            ship['life']-=shot['damage']
            if (ship['enemy']):
                ship['blinking_time'] = 0
            del shots[n]

def player_death(player):
    if (player['life']<1):
        pygame.mixer.Sound.play(player['explosion_sound'])
        return GAME_STATE_DEATH

def enemies_death(enemies_spawned, power_ups, power_ups_spawned):
    for n in range(len(enemies_spawned)-1, -1, -1):
        enemy_spawn=enemies_spawned[n]
        if enemy_spawn['life'] < 1:
            posibility = random.randint(1, 2)
            if (posibility == 1):
                power_ups_spawned.append(spawn_power_up(enemy_spawn, power_ups['blue_shot']))
            pygame.mixer.Sound.play(enemy_spawn['explosion_sound'])
            del enemies_spawned[n]
            

def spawn_power_up(enemy_spawn, power_up):
    x = enemy_spawn['x'] - enemy_spawn['x_power_up_position']
    y = enemy_spawn['y'] - enemy_spawn['y_power_up_position']
    power_up={
        'x': x,
        'y': y,
        'sprite': power_up['sprite'],
        'collision': pygame.Rect(2, 1, 36, 38),
        'pick_up_sound': power_up['pick_up_sound']
    }
    return power_up

def draw_overlay(screen, font, player):
    font.render_to(screen, (30, 550), 'Life: ' + str(player['life']), (WHITE))

def create_power_ups():
    power_ups={
        'speed': 0.4,
        'blue_shot': {
            'sprite': pygame.image.load('images/power_ups/power_shot.png'),
            'damage': 2,
            'pick_up_sound': pygame.mixer.Sound("sounds/effects/power_up_sound_effect.wav")
        }
    }
    return power_ups

def draw_power_ups(screen, power_ups_spawned):
    for power_up_spawned in power_ups_spawned:
        sprite=power_up_spawned['sprite']
        square=sprite.get_rect().move(power_up_spawned['x'], power_up_spawned['y'])
        screen.blit(sprite, square)


def apply_power_ups_to_player(ship, power_ups_spawned):
    if (ship['moved'] == False):
        sprite=ship['idle']
    else:
        sprite=ship['sprites'][ship['sprite_index']]
    ship_square=sprite.get_rect().move(ship['x'], ship['y'])
    for n in range(len(power_ups_spawned)-1, -1, -1):
        power_up_spawned=power_ups_spawned[n]
        power_up_square=power_up_spawned['collision'].move(power_up_spawned['x'], power_up_spawned['y'])
        if ship_square.colliderect(power_up_square):
            ship['power_shot_bullets'] = ship['power_shot_bullets']+2
            pygame.mixer.Sound.play(power_up_spawned['pick_up_sound'])
            del power_ups_spawned[n]

def move_power_ups(power_ups_spawned):
    for n in range(len(power_ups_spawned)-1, -1, -1):
        power_up_spawned=power_ups_spawned[n]
        power_up_spawned['y']=power_up_spawned['y']-POWER_UP_SPEED

def elements_out_of_screen(elements):
    for n in range(len(elements)-1, -1, -1):
        element=elements[n]
        if element['y'] < POSITION_TO_DELETE_PLAYER_SHOT_Y_TOP or element['y'] > POSITION_TO_DELETE_PLAYER_SHOT_Y_BOTTOM:
            del elements[n]

def draw_power_shot_count(screen, power_shot_bullets, font):
    font.render_to(screen, (530, 550), "Power Bullets: " + str(power_shot_bullets), (WHITE))


def draw_wave(screen, font):
    font.render_to(screen, (315, 100), "Wave1", (WHITE))

def draw_death(screen):
    sprite=pygame.image.load('images/menu/game_over.png')
    square=sprite.get_rect().move(139, 107)
    screen.blit(sprite, square)

def draw_victory(screen, font):
    font.render_to(screen, (315, 150), 'You won', (WHITE))

def check_victory(enemies_spawned, waves):
    if (len(enemies_spawned)==0 and len(waves)==0):
        return True
    else:
        return False      

def game_menu(resources):
    start_btn_light=resources['start_btn_light']
    start_btn_dark=resources['start_btn_dark']
    exit_btn_light=resources['exit_btn_light']
    exit_btn_dark=resources['exit_btn_dark']
    background=resources['menu_background']
    screen=resources['screen']
    title=resources['title']

    start_btn=start_btn_light
    exit_btn=exit_btn_light
    
    going=True
    while going:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                square=start_btn.get_rect().move(150, 500)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_PLAYING
                square=exit_btn.get_rect().move(450, 500)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_EXIT
        square=start_btn.get_rect().move(150, 500)
        if square.collidepoint(pygame.mouse.get_pos()):
            start_btn=start_btn_dark
        else:
            start_btn=start_btn_light
        square=exit_btn.get_rect().move(450, 500)
        if square.collidepoint(pygame.mouse.get_pos()):
            exit_btn=exit_btn_dark
        else:
            exit_btn=exit_btn_light
        screen.blit(background, background.get_rect())
        screen.blit(title, title.get_rect().move(400-360, 50))
        screen.blit(start_btn, start_btn.get_rect().move(150,500))
        screen.blit(exit_btn, exit_btn.get_rect().move(450,500))
        pygame.display.flip()
    return result

def load_resources():
    resources = {}
    resources['game_icon']=pygame.image.load('images/icon.png')
    resources['screen'] = pygame.display.set_mode([WINDOW_SIZE_X, WINDOW_SIZE_Y])
    resources['title']=pygame.image.load('images/menu/space_killer_title.png')
    resources['start_btn_light']=pygame.image.load('images/menu/start_button.png')
    resources['start_btn_dark']=pygame.image.load('images/menu/start_button_dark.png')
    resources['exit_btn_light']=pygame.image.load('images/menu/exit_button.png')
    resources['exit_btn_dark']=pygame.image.load('images/menu/exit_button_dark.png')
    resources['menu_background']=pygame.image.load('images/menu/background.png')
    resources['overlay_font'] = pygame.freetype.Font("fonts/Lato-Black.ttf", 32)
    return resources
            
def main():
    pygame.init()
    game_icon = pygame.image.load('images/icon.png') #Obtener imagen de icono
    pygame.display.set_caption(GAME_TITLE) # Setear titulo de la ventana de juego
    pygame.display.set_icon(game_icon) # Setear icono de la ventana del juego
    resources=load_resources()
    game_state=GAME_STATE_MENU
    while game_state!=GAME_STATE_EXIT:
        if game_state==GAME_STATE_MENU:
            game_state=game_menu(resources)
        elif game_state==GAME_STATE_PLAYING:
            game_state=game_loop()
    pygame.quit()

def game_loop():
    screen = pygame.display.set_mode([WINDOW_SIZE_X, WINDOW_SIZE_Y]) #Crear pantalla de juego
    clock = pygame.time.Clock()
    player=create_player()
    overlay_font = pygame.freetype.Font("fonts/Lato-Black.ttf", 32)
    enemies=create_enemies()
    waves=create_waves()
    power_ups=create_power_ups()
    player_shots_spawned=[]
    enemies_spawned=[]
    enemies_shots=[]
    power_ups_spawned=[]
    background=pygame.image.load('images/background/background.png')
    result=GAME_STATE_PLAYING
    going=True
    while going:
        delta=clock.tick(FPS)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN and (result==GAME_STATE_DEATH or result==GAME_STATE_VICTORY):
                going=False
                result=GAME_STATE_MENU
        if (result==GAME_STATE_DEATH or result==GAME_STATE_EXIT or result==GAME_STATE_MENU or result==GAME_STATE_VICTORY):
            continue
        player['time_since_last_shot']+=delta
        player['time_since_last_hit']+=delta
        for enemy_spawn in enemies_spawned:
            enemy_spawn['time_since_last_shot'] += delta
            enemy_spawn['time_since_last_hit'] += delta
            enemy_spawn['blinking_time'] += delta
            hit_ship(enemy_spawn, player_shots_spawned)
        enemies_death(enemies_spawned, power_ups, power_ups_spawned)
        apply_power_ups_to_player(player, power_ups_spawned)
        result=player_death(player)
        screen.blit(background, background.get_rect())
        move_player(player, delta)
        hit_ship(player, enemies_shots)
        draw_player(screen, player)
        draw_power_ups(screen, power_ups_spawned)
        draw_power_shot_count(screen, player['power_shot_bullets'], overlay_font)
        check_waves(waves, enemies, enemies_spawned)
        aproach_enemies(enemies_spawned)
        move_enemies(enemies_spawned)
        move_power_ups(power_ups_spawned)
        draw_enemies(screen, enemies_spawned)
        player_shot(screen, player, player_shots_spawned)
        shot_move(player_shots_spawned)
        shot_move(enemies_shots)
        enemies_shots_function(screen, enemies_spawned, enemies_shots)
        elements_out_of_screen(player_shots_spawned)
        elements_out_of_screen(enemies_shots)
        elements_out_of_screen(power_ups_spawned)
        draw_overlay(screen, overlay_font, player)
        if (check_victory(enemies_spawned, waves)==True):
            draw_victory(screen, overlay_font)
            result=GAME_STATE_VICTORY
        if result==GAME_STATE_DEATH:
            draw_death(screen)
        pygame.display.flip()
    return result
main()

# Limitar espacio de movimiento del jugador por la parte de arriba de la pantalla

# TIPS
# Poner posiciones predeterminadas pero que sea aleatorio que aparezcan ahi o no

#EXTRAS
# Responsiveness de los enemigos al recibir daño